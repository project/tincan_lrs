<?php
/**
 * @file
 * Class for custom handling of JSON responses for the Tincan Server
 */

/**
 * Provides ServicesParserTextPlain
 */
class ServicesApplicationOctetStreamParser implements ServicesParserInterface {
  /**
   * @param ServicesContextInterface $context
   * @return mixed
   */
  public function parse(ServicesContextInterface $context) {
    //watchdog('octet-parser', print_r($context->getRequestBody(), TRUE));

    return $context->getRequestBody();

  }
}
