<?php
/**
 * @file
 * Class for custom handling of JSON responses for the Tincan Server
 */

/**
 * Provides ServicesTinCanJSONFormatter
 */
class ServicesTinCanJSONFormatter extends ServicesJSONFormatter {
  /**
   * {@inheritdoc}
   */
  public function render($data) {
    // json_encode doesn't give valid json with data that isn't an array/object.
    if (is_scalar($data)) {
      $data = array($data);
    }
    // work around to allow binary non formatted results
    if(is_array($data) && isset($data['binary'])) {
      return trim($data['binary']);
    }
    else {
      $return = json_encode($data);
      $return = str_replace('\\/', '/', $return);
      $return = str_replace('\"', '"', $return);
      return $return;
    }
  }
}
