<?php
/**
 * @file
 * Class for custom handling of octet-stream responses for the Tincan Server
 */

/**
 * Provides ServicesTinCanOctetStreamFormatter
 */
class ServicesTinCanOctetStreamFormatter implements ServicesFormatterInterface {
  /**
   * {@inheritdoc}
   */
  public function render($data) {
    // work around to allow binary non formatted results
    if(is_array($data) && isset($data['binary'])) {
      return trim($data['binary']);
    }
    else return trim($data);
  }
}
