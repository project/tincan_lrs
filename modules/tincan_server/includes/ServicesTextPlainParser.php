<?php
/**
 * @file
 * Class for custom handling of JSON responses for the Tincan Server
 */

/**
 * Provides ServicesParserTextPlain
 */
class ServicesParserTextPlain implements ServicesParserInterface {
  /**
   * @param ServicesContextInterface $context
   * @return mixed
   */
  public function parse(ServicesContextInterface $context) {
    //watchdog('textparser', print_r($context->getRequestBody(), TRUE));
    //watchdog('textparser', print_r($context->getPostData(), TRUE));
    //watchdog('textparser', print_r($context->getRequestBodyData(), TRUE));

    //return $context->getPostData();
    return $context->getRequestBody();
    //return json_decode($context->getRequestBody(), TRUE);
  }
}
