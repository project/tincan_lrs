<?php
/**
 * @file
 * Access and HTTP method handlers for the "agents" resource.
 *
 * Handles access and handling for the "agents" and "agents/profile" APIs
 */

/**
 * Access callback for "agents" resource, index method.
 *
 * Callback for hook_services_resources().
 *
 * @return Boolean
 * TRUE if user has 'access agents GET api' permission
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_agents_index_handler_access() {
  return user_access('access agents GET api');
}

/**
 * Callback for "agents" resource, index method.
 *
 * Callback for hook_services_resources().
 *
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws a ServicesException, with message and code depending on conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_agents_index_handler($params) {
 _tincan_lrs_agents_get_processor($params);
}

/**
 * Access callback for "agents" resource, 'retrieve' or GET method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none or profile)
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_agents_get_handler_access($op, $args) {
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'profile':
        return user_access('access agent profiles GET api');
      default :
        return user_access('access agents GET api');
    }
  }
  else return user_access('access agents GET api');
}

/**
 * Callback for "agents" resource, retrieve (GET) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $agentId
 *  This is either an agentId for the "agents" API
 *  Or (profile) to access the Agent Profile APIs
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_agents_get_handler($agentId, $params) {
  switch ($agentId) {
    case 'profile':
      _tincan_lrs_agent_profiles_get_processor($params);
      break;
    default:
      _tincan_lrs_agents_get_processor($params, $agentId);
  }
}

/**
 * Access callback for "agents" resource, 'update' or PUT method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none or profile)
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_agents_put_handler_access($op, $args) {
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'profile':
        return user_access('access agent profiles PUT api');
      default :
        services_error('Not Found', 404, 'not allowed to PUT agents via API');
    }
  }
  else return FALSE;
}

/**
 * Callback for "agents" resource, update (PUT) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $type
 *   First argument tell you which API is being accessed (state or profile)
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_agents_put_handler($type, $params, $data) {
  // $content = drupal_json_encode($data);
  // got a problem here, services is expecting json in the data of the http request
  // when it doesn't parse any json, it gives nothing....
  // so usign Poster, setting Content Type to application/json, makes services expect json
  // and returns nothing if it is not json
  // not really sure how to test this ATM
  // when url parameters are given the $param array is populated with values
  // when parameters are set via the Parameters in Poster, it is not populated 
  $headers = array();
  foreach ($_SERVER as $key => $value) {
    $length = 5;
    //$header = _services_fix_header_key($key, $length);
    $header = $key;
    $headers[$header] = $value;
  }
  // watchdog('agent profiles put handler', 'header: <pre>%d</pre>', array('%d' => print_r($headers,1)), WATCHDOG_DEBUG);
  $content = $data;
  if ($type == 'profile') {
    _tincan_lrs_agent_profiles_put_processor($params, $content);
  }
  else services_error('Not Found', 404, 'not allowed to PUT agents via API');
}

/**
 * Access callback for "agents" resource, 'create' or POST method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none or profile)
 *
 * @ingroup callbacks
 */
function _tincan_lrs_agents_post_handler_access($op, $args) {
  $method = isset($args[1]['method']) ? $args[1]['method'] : FALSE;
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'profile':
        if ($method) {
          switch ($method) {
            case 'GET':
              return user_access('access agent profiles GET api');
            case 'PUT':
              return user_access('access agent profiles PUT api');
            case 'POST':
              return user_access('access agent profiles POST api');
            case 'DELETE':
              return user_access('access agent profiles DELETE api');
            default:
              services_error("Bad Request", 400, 'Invalid method parameter value');
          }
        }
        else return user_access('access agent profiles POST api');
      default :
        services_error('Not Found', 404, 'not allowed to POST Agents via API');
    }
  }
  else services_error('Not Found', 404, 'not allowed to POST Agents via API');
}

/**
 * Callback for "agents" resource, create (POST) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $type
 *   First argument tell you which API is being accessed (profile)
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_agents_post_handler($type, $params, $data) {
  $entityBody = file_get_contents('php://input',TRUE);

  // when url parameters are given the $param array is populated with values
  // when parameters are set via the Parameters in Poster, it is not populated 

  $query = drupal_json_decode(urldecode($entityBody));
  $headers = array();
  foreach ($_SERVER as $key => $value) {
    $length = 5;
    //$header = _services_fix_header_key($key, $length);
    $header = $key;
    $headers[$header] = $value;
  }
  // watchdog('agent profiles post handler', 'header: <pre>%d</pre>', array('%d' => print_r($headers,1)), WATCHDOG_DEBUG);  
  $content_array = array();
  // https://github.com/adlnet/xAPI-Spec/blob/master/xAPI.md#78-cross-origin-requests
  if (isset($params['method']) && $params['method'] != '') {
    $query = drupal_get_query_array($entityBody);
    $profileId = isset($query['profileId']) ? $query['profileId'] : FALSE;
    if (isset($query['Authorization']) ) {
      $auth = $query['Authorization'];
      unset($query['Authorization']);
    }
    if (isset($query['X-Experience-API-Version']) ) {
      $xapi_version = $query['X-Experience-API-Version'];
      unset($query['X-Experience-API-Version']);
    }
    $content_type = isset($query['Content-Type']) ? $query['Content-Type'] : FALSE;
    $content = isset($query['content']) ? $query['content'] : FALSE;
    if ($content) {
      $content_array = drupal_json_decode($content);
    }
    $method = $params['method'];
    unset($params['method']);
     //now shoot the results off to the handlers for the ?method parameter --
      if ($type == 'profile') {
        switch ($method) {
          case 'GET':
            _tincan_lrs_agent_profiles_get_processor($query, $content,$profileId);
          break;
          case 'PUT':
            _tincan_lrs_agent_profiles_put_processor($query, $content, $profileId);
          break;
          case 'POST':
            _tincan_lrs_agent_profiles_post_processor($query, $content);
          break;
          case 'DELETE':
            _tincan_lrs_agent_profiles_delete_processor($query, $content, $profileId);
          break;
          default:
            services_error("Bad Request", 400, 'Invalid method parameter value');
        } // end switch($method)
      } //end if type = profile
      else {
        services_error('Not Found', 404, 'not allowed to PUT agents via API');
      } 
  } //end if method
  else {
    if ($type == 'profile') {
      // $content = drupal_json_encode($data);
       $content = $data;
       _tincan_lrs_agent_profiles_post_processor($params, $content);
    }
    else  services_error('Not Found', 404, 'not allowed to PUT agents via API');
  }
}

/**
 * Access callback for "agents" resource, 'delete' or DELETE method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none or profile)
 *
 * @throws ServicesException
 *  Throws ServicesException if the API receives DELETE requests to the "activities" API 
 * @ingroup callbacks
 */
function _tincan_lrs_agents_delete_handler_access($op, $args) {
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'profile':
        return user_access('access agent profiles DELETE api');
      default :
        services_error('Not Found', 404, 'No DELETEing agents via API');
    }
  }
  else services_error('Not Found', 404, 'No DELETEing agents via API');
}

/**
 * Callback for "agents" resource, delete (delete) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $type
 *   First argument tell you which API is being accessed (profile)
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_agents_delete_handler($type, $params, $data) {
  if($type == 'profile') {
    _tincan_lrs_agent_profiles_delete_processor($params,$data);
  }
  else {
    services_error('Not Found', 404, 'No DELETEing activities via API');;
  }
}