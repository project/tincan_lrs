<?php
/**
 * @file
 * Access and index method handler for the "about" resource.
 */

/**
 * Access callback for "about" resource.
 *
 * Callback for hook_services_resources().
 *
 * @return Boolean
 * TRUE 
 *
 * @ingroup callbacks
 */
function _tincan_lrs_about_index_handler_access() {
  return TRUE;
}

/**
 * Callback for "about" resource.
 *
 * Callback for hook_services_resources().
 *
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @return array
 *   Returns array to be encoded to JSON
 *
 * @ingroup callbacks
 */
function _tincan_lrs_about_index_handler($params) {
  $about= array(
    'version' => array(
      '1.0.0',
    ),
    'extensions' => new stdClass(),
  );
  return drupal_json_encode($about);
}
