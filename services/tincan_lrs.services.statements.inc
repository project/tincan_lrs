<?php
/**
 * @file
 * Access and HTTP method handlers for the "statements" resource.
 *
 * Handles access and handling for the Statements API
 */

/**
 * Access callback for "agents" resource, index and retrieve (GET) method.
 *
 * Callback for hook_services_resources().
 *
 * @return Boolean
 * TRUE if user has 'access statements GET api' permission
 *
 * @ingroup callbacks
 */
function _tincan_lrs_statements_get_handler_access($op, $args) {
  return user_access('access statement GET api');
}

/**
 * Callback for "statements" resource, index method.
 *
 * Callback for hook_services_resources().
 *
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws a ServicesException, with message and code depending on conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_statements_index_handler($params) {
  _tincan_lrs_statement_get_processor($params);
}

/**
 * Callback for "statements" resource, retrieve (GET) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $statementId
 *   statementId parameter for the Statements API
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_statements_get_handler($statementId = FALSE, $params) {
  if ($statementId) {
    if (is_array($params)) $params['statementId'] = $statementId;
    else $params = array('statementId' => $statementId);   
  }
  _tincan_lrs_statement_get_processor($params);
}

/**
 * Access callback for "statements" resource, 'update' or PUT method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission 'access statement PUT api'
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_statements_put_handler_access($op, $args) {
  return user_access('access statement PUT api');
}

/**
 * Callback for "statements" resource, update (PUT) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $statementId
 *   optional $statementId 
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_statements_put_handler($statementId = FALSE, $params, $data) {
  $entityBody = file_get_contents('php://input',TRUE);
  $content = drupal_json_encode($data);
  _tincan_lrs_statement_put_processor($content, $params);
}

/**
 * Access callback for "statements" resource, 'create' or POST method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate permissions for the HTTP method of the request
 *
 * @ingroup callbacks
 */
function _tincan_lrs_statements_post_handler_access($op, $args) {
 // check for method parameter and use permissions for those if it is there...
  if (isset($args[2]['method'])) {
    switch ($args[2]['method']) {
      case 'GET':
        return user_access('access statement GET api');
      case 'PUT':
        return user_access('access statement PUT api');
      case 'POST':
        return user_access('access statement POST api');
      case 'DELETE':
        return FALSE;
    }
  }
  else {
    return user_access('access statement POST api');
  }
}

/**
 * Callback for "statements" resource, create (POST) method.
 *
 * Callback for hook_services_resources().
 *
 * @param array $data
 *  Array of parsed HTTP Request body values
 * @param string $method
 *   First argument tell you which API is being accessed (profile)
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_statements_post_handler($data, $method, $params) {
  $entityBody = file_get_contents('php://input',TRUE);

  $content_array = array();
  // https://github.com/adlnet/xAPI-Spec/blob/master/xAPI.md#78-cross-origin-requests
  if (isset($method) && $method != '') {
    //$entityBody = urldecode($entityBody);

    $query = drupal_get_query_array($entityBody);

    $content = isset($query['content']) ? $query['content'] : FALSE;
    $statementId = isset($query['statementId']) ? $query['statementId'] : FALSE;
    $auth = isset($query['Authorization']) ? $query['Authorization'] : FALSE;
    $xapi_version = isset($query['X-Experience-API-Version']) ? $query['X-Experience-API-Version'] : FALSE;
    $content_type = isset($query['Content-Type']) ? $query['Content-Type'] : FALSE;
    if(!empty($content)) {
      $content_array = drupal_json_decode($content);
    }
     //now shoot the results off to the handlers for the ?method parameter --
    unset($params['method']);
    switch ($method) {
      case 'GET':
        _tincan_lrs_statement_get_processor(urldecode($content), $params, $statementId);
      break;
      case 'PUT':
        _tincan_lrs_statement_put_processor(urldecode($content), $params, $statementId);
      break;
      case 'POST':
        _tincan_lrs_statement_post_processor(urldecode($content), $params, $statementId);
      break;
      case 'DELETE':
        services_error('Bad Request', 400);
      break;
      case 'none':
      default:
        $content = drupal_json_encode($data);
        _tincan_lrs_statement_post_processor($content, $params);  
    } // end switch($method)
  } //end if method
}

