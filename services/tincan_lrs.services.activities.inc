<?php
/**
 * @file
 * Access and HTTP method handlers for the "activities" resource.
 *
 * Handles access and handling for the "activities", "activities/state", and "activities/profile" APIs
 */

/**
 * Access callback for "activities" resource, index method.
 *
 * Callback for hook_services_resources().
 *
 * @return Boolean
 * TRUE if user has 'access activities GET api' permission
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_activities_index_handler_access($op, $args) {
  return user_access('access activities GET api');
}

/**
 * Callback for "activities" resource, index method.
 *
 * Callback for hook_services_resources().
 *
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws a ServicesException, with message and code depending on conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_activities_index_handler($params) {
  _tincan_lrs_activity_get_processor($params);
}

/**
 * Access callback for "activities" resource, 'retrieve' or GET method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none, state, or profile)
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_activities_get_handler_access($op, $args) {
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'state':
        return user_access('access activities state GET api');
      case 'profile':
        return user_access('access activity profiles GET api');
      default :
        return user_access('access activities GET api');
    }
  }
  else return user_access('access activities GET api');
}

/**
 * Callback for "activities" resource, retrieve (GET) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $activityId
 *  This is either an activityId for the "activities" API
 *  Or (state or profile) to access the State and Activity Profile APIs
 * @param array $params
 *   Array of HTTP request parameters
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_activities_get_handler($activityId, $params) {
  //  when url parameters are given the $param array is populated with values
  //  when parameters are set via the Parameters in Poster, it is also populated with values
  switch ($activityId) {
    case 'profile':
      _tincan_lrs_activity_profiles_get_processor($params);
      break;
    case 'state':
      _tincan_lrs_activity_state_get_processor($params);
      break;
    default:
      _tincan_lrs_activity_get_processor($params, $activityId);
  
  }
}

/**
 * Access callback for "activities" resource, 'update' or PUT method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none, state, or profile)
 *
 * @ingroup callbacks
 */ 
function _tincan_lrs_activities_put_handler_access($op, $args) {
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'state':
        return user_access('access activities state PUT api');
      case 'profile':
        return user_access('access activity profiles PUT api');
      default :
        services_error('Not Found', 404, 'not allowed to PUT activities via API');
    }
  }
  else return FALSE;
}

/**
 * Callback for "activities" resource, update (PUT) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $type
 *   First argument tell you which API is being accessed (state or profile)
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_activities_put_handler($type, $params, $data) {
  //$content = drupal_json_encode($data);
  
  // got a problem here, services is expecting json in the data of the http request
  // when it doesn't parse any json, it gives nothing....
  // so usign Poster, setting Content Type to application/json, makes services expect json
  // and returns nothing if it is not json
  // not really sure how to test this ATM
  
  // this doesn't seem to work
  // $content = file_get_contents('php://input',TRUE);

  $content = $data;
  // when url parameters are given the $param array is populated with values
  // when parameters are set via the Parameters in Poster, it is not populated 


  if ($type == 'state') {
    _tincan_lrs_activity_state_put_processor($params, $content);
  }
  elseif ($type == 'profile') {
   _tincan_lrs_activity_profiles_put_processor($params, $content);
  }
  else services_error('Not Found', 404, 'not allowed to PUT activities via API');
}

/**
 * Access callback for "activities" resource, 'create' or POST method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none, state, or profile)
 *
 * @ingroup callbacks
 */
function _tincan_lrs_activities_post_handler_access($op, $args) {
  $method = isset($args[1]['method']) ? $args[1]['method'] : FALSE;
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'state':
        if ($method) {
          switch ($method) {
            case 'GET':
              return user_access('access activities state GET api');
            case 'PUT':
              return user_access('access activities state PUT api');
            case 'POST':
              return user_access('access activities state POST api');
            case 'DELETE':
              return user_access('access activities state DELETE api');
            default:
              return services_error("Bad Request", 400, 'Invalid method parameter value');
          }
        }
        else return user_access('access activities state POST api');
      case 'profile':
        if ($method) {
          switch ($method) {
            case 'GET':
              return user_access('access activity profiles GET api');
            case 'PUT':
              return user_access('access activity profiles PUT api');
            case 'POST':
              return user_access('access activity profiles POST api');
            case 'DELETE':
              return user_access('access activity profiles DELETE api');
            default:
              services_error("Bad Request", 400, 'Invalid method parameter value');
          }
        }
        else return user_access('access activity profiles POST api');
      default :
        services_error('Not Found', 404, 'not allowed to POST activities via API');
    }
  }
  else services_error('Not Found', 404, 'not allowed to POST activities via API');
}

/**
 * Callback for "activities" resource, create (POST) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $type
 *   First argument tell you which API is being accessed (state or profile)
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_activities_post_handler($type, $params, $data) {
  $entityBody = file_get_contents('php://input',TRUE);

  // got a problem here, services is expecting json in the data of the http request
  // when it doesn't parse any json, it gives nothing....
  // so usign Poster, setting Content Type to application/json, makes services expect json
  // not really sure how to test this ATM
  // $data is empty unless its json when using Poster...
  
 
  // when url parameters are given the $param array is populated with values
  // when parameters are set via the Parameters in Poster, it is not populated 
   
  $query = drupal_json_decode(urldecode($entityBody));
  $headers = array();
  foreach ($_SERVER as $key => $value) {
    $length = 5;
    //$header = _services_fix_header_key($key, $length);
    $header = $key;
    $headers[$header] = $value;
  }
  
  $content_array = array();
  // https://github.com/adlnet/xAPI-Spec/blob/master/xAPI.md#78-cross-origin-requests
  if (isset($params['method']) && $params['method'] != '') {
    $query = drupal_get_query_array(urldecode($entityBody));
    $stateId = isset($query['stateId']) ? $query['stateId'] : FALSE;
    $profileId = isset($query['profileId']) ? $query['profileId'] : FALSE;
    if (isset($query['Authorization']) ) {
      $auth = $query['Authorization'];
      unset($query['Authorization']);
    }
    if (isset($query['X-Experience-API-Version']) ) {
      $xapi_version = $query['X-Experience-API-Version'];
      unset($query['X-Experience-API-Version']);
    }
    $content_type = isset($query['Content-Type']) ? $query['Content-Type'] : FALSE;
    $content = isset($query['content']) ? $query['content'] : FALSE;
    if ($content) $content_array = drupal_json_decode($content);
    $method = $params['method'];
    unset($params['method']);
     //now shoot the results off to the handlers for the ?method parameter --
    if ($type == 'state') {  
      switch ($method) {
        case 'GET':
          _tincan_lrs_activity_state_get_processor($query, $stateId);
        break;
        case 'PUT':
          _tincan_lrs_activity_state_put_processor($query, $content, $stateId);
        break;
        case 'POST':
          _tincan_lrs_activity_state_post_processor($query, $content, $stateId);
        break;
        case 'DELETE':
          _tincan_lrs_activity_state_delete_processor($query, $content, $stateId);
        break;
      } // end switch($method)
    } //end if type = state
    elseif ($type == 'profile') {
      switch ($method) {
        case 'GET':
          _tincan_lrs_activity_profiles_get_processor($query, $content,$profileId);
        break;
        case 'PUT':
          _tincan_lrs_activity_profiles_put_processor($query, $content, $profileId);
        break;
        case 'POST':
          _tincan_lrs_activity_profiles_post_processor($query, $content, $profileId);
        break;
        case 'DELETE':
          _tincan_lrs_activity_profiles_delete_processor($query, $content, $profileId);
        break;
      } // end switch($method)
    } //end if type = profile
    else {
      services_error('Not Found', 404, 'No POSTing to activities API');
    }
  } //end if method
  else {
    if($type == 'state') {
       //$content = drupal_json_encode($data);
       $content = $data;
       _tincan_lrs_activity_state_post_processor($params, $content);
    }
    elseif($type == 'profile') {
       //$content = drupal_json_encode($data);
       $content = $data;
       _tincan_lrs_activity_profiles_post_processor($params, $content);
    }
    else services_error('Not Found', 404, 'No POSTing to activities API');
  }
}

/**
 * Access callback for "activities" resource, 'delete' or DELETE method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $op
 *   HTTP Operation (method)
 * @param array $args
 *  Array of arguments 
 * 
 * @return Boolean
 * TRUE if user has permission appropriate to the url argument (none, state, or profile)
 *
 * @throws ServicesException
 *  Throws ServicesException if the API receives DELETE requests to the "activities" API 
 * @ingroup callbacks
 */
function _tincan_lrs_activities_delete_handler_access($op, $args) {
  if (isset($args[0])) {
    switch ($args[0]) {
      case 'state':
        return user_access('access activities state DELETE api');
      case 'profile':
        return user_access('access activity profiles DELETE api');
      default :
        services_error('Not Found', 404, 'No DELETEing activities via API');
    }
  }
  else services_error('Not Found', 404, 'No DELETEing activities via API');
}

/**
 * Callback for "activities" resource, delete (delete) method.
 *
 * Callback for hook_services_resources().
 *
 * @param string $type
 *   First argument tell you which API is being accessed (state or profile)
 * @param array $params
 *   Array of HTTP request parameters
 * @param array $data
 *  Array of parsed HTTP Request body values
 *
 * @throws ServicesException
 *   Throws various ServicesExceptions based on the conditions and results
 *
 * @ingroup callbacks
 */
function _tincan_lrs_activities_delete_handler($type, $params, $data) {
  if($type == 'state') {
    _tincan_lrs_activity_state_delete_processor($params,$data);
  }
  elseif($type == 'profile') {
    _tincan_lrs_activity_profiles_delete_processor($params,$data);
  }
}
