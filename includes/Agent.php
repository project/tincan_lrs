<?php
/**
 * @file
 * Defines TincanAgents entity object as well as all the associated controller objects 
 */

/**
 * Represents a Tincan Agent
 */
class TincanAgent extends Entity {
  private $notation;

 /**
  * Creates an agent entity
  *
  * @param array $json_array
  *   Array of values parsed from JSON
  *
  * @return integer
  *   Returns the entity id of the agent if created successfully, otherwise 0;
  */ 
  private function createMember($json_array) {
    $values = array();
    $target_id = 0;
    $json = drupal_json_encode($json_array);
    $values['json'] = $json;
      
    $tincan_agent_entity = tincan_lrs_agent_create($values);
    $tincan_agent_entity->populateEntityValues();
    try {
      $save_result = $tincan_agent_entity->save();
    }
    catch (Exception $e) {
      services_error ('Internal Server Error', 500, 'Agent member create save fail: ' . $e->getMessage());
    }
    if ($save_result) {
      $target_id = $tincan_agent_entity->id;
    }
    return $target_id;
  }

  /**
   * Finds an agent entity
   *
   * @param array $json_array
   *   Array of values parsed from JSON
   *
   * @return integer
   *   Returns the entity id of the agent if found, otherwise 0;
   */
  private function findMember($json_array) {
    if (!isset($json_array['mbox']) &&
       !isset($json_array['mbox_sha1sum']) &&
       !isset($json_array['openid']) &&
       (!isset($json_array['account']) && !isset($json_array['account']['homePage']) && ! isset($json_array['account']['name'])) ) {
      return 0;  
    }

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type','tincan_agent');
    $query->propertyCondition('object_type','Agent');

    $ifi_found = 0;

    if (isset($json_array['mbox'])) {
      $query->propertyCondition('mbox',$json_array['mbox']);
      $ifi_found = 1;
    }
    if (isset($json_array['mbox_sha1sum']) && !$ifi_found) {
      $query->propertyCondition('mbox_sha1sum',$json_array['mbox_sha1sum']);
      $ifi_found = 1;
    }
    if (isset($json_array['openid']) && !$ifi_found) {
      $query->propertyCondition('openid',$json_array['openid']);
      $ifi_found = 1;
    }
    if (isset($json_array['account']) && isset($json_array['account']['homePage']) && isset($json_array['account']['name']) && !$ifi_found) {
      $query->propertyCondition('account_home_page',$json_array['account']['homePage']);
      $query->propertyCondition('account_name',$json_array['account']['name']);
      $ifi_found = 1;
    }
    $result = $query->execute();

    if (isset($result['tincan_agent'])) {
      foreach ($result['tincan_agent'] as $key => $agent) {
        return $key;
      }
    }
    else return 0;
  }

 /**
  * Populates the entity properties and fields from set entity JSON
  */ 
  private function populate() {
    $json = $this->notation;
    $json_array = drupal_json_decode($json);

    // objectType
    if (isset($json_array['objectType'])) {
      $this->object_type = $json_array['objectType'];
    }
    else {
      $this->object_type = 'Agent';
    }
    // name
    if (isset($json_array['name'])) {
      $this->name = $json_array['name'];
    }
    $ifi_found = 0;
    // mbox
    if (isset($json_array['mbox'])) {
      $this->mbox= $json_array['mbox'];
      $ifi_found = 1;
    }
    // mbox_sha1sum
    if (isset($json_array['mbox_sha1sum']) && !$ifi_found) {
      $this->mbox_sha1sum = $json_array['mbox_sha1sum'];
      $ifi_found = 1;
    }
    // openid
    if (isset($json_array['openid']) && !$ifi_found) {
      $this->openid= $json_array['openid'];
      $ifi_found = 1;
    }
    // account
    if (isset($json_array['account']) && isset($json_array['account']['homePage']) && isset($json_array['account']['name']) && !$ifi_found) {
      // homepage
      $this->account_home_page = $json_array['account']['homePage'];
      // account name
      $this->account_name = $json_array['account']['name'];
      $ifi_found = 1;
    }

    //members
    // TODO
     if (isset($json_array['member'])) {
       if (!isset($json_array['member'][0]) ) {
         $target_id = $this->findMember($json_array['member']);
         if (!$target_id) {
           $target_id = $this->createMember($json_array['member']);
         }
         if ($target_id) {
           $this->tincan_group_members[LANGUAGE_NONE][0]['target_id'] = $target_id;
         }
       }
       else {
         $count  = 0;
         foreach ($json_array['member'] as $item) {
           $target_id = $this->findMember($item);
           if (!$target_id) {
             $target_id = $this->createMember($item);
           }
           if ($target_id) {
             $this->tincan_group_members[LANGUAGE_NONE][$count]['target_id'] = $target_id;
           }
           $count += 1;
         }
       }
     } //end if isset members
  } //end populate method

 /**
  * Constructs a TincanAgent entity
  */
  public function __construct($values = array()) {
    parent::__construct($values, 'tincan_agent');
    $this->notation = isset($values['json']) ? $values['json'] : '';
  }

 /**
  * Returns the default label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */
	protected function defaultLabel() {
    if(isset($this->name) && $this->name != '') {
      return $this->name;
    }
    return $this->mbox;
  }

 /**
  * Returns the label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */ 
  function label() {
    return $this->defaultLabel();
  }

 /**
  * Returns the default URI for the entity
  *
  * @return string
  *   Returns the URI for the entity
  */
  protected function defaultUri() {
    return array('path' => 'tincan-agents/' . $this->id);
  }
  
 /**
  * Provides an array of the decoded JSON for the entity, from the received statement
  *
  * @return string
  *   Returns array of Decoded JSON of the entity
  */
  function toArray() {
     return drupal_json_decode($this->notation);
  }

 /**
  * Provides received statement JSON for the entity
  *
  * @return string
  *   Returns the label for the entity
  */
  function getJSON() {
    return $this->notation;
  }

 /**
  * Sets the working JSON for the entity
  */  
  function setJSON($json) {
    $this->notation = $json;
  }

 /**
  * Validates the JSON for the entity
  *
  * @return Boolean
  *   Returns TRUE if the JSON validates, otherwise FALSE
  */ 
  function validateJSON() {
    if($this->notation == '') {
      return FALSE;
    }
    return _tincan_lrs_basic_json_validation($this->notation,'tincan_agent entity validation');
  }

 /**
  * Populates the properties and fields for the entity from the decoded entity JSON
  */ 
  function populateEntityValues() {
    if($this->notation == '') return FALSE;
    if($this->validateJSON()) {
      //process and populate entity
      $this->populate();
    }
  }

 /**
  * Produces JSON for the TincanAgent entity in the specified format
  *
  * @param string $format
  *   String specifing the format of JSON to produce
  *   Accepted values of 'exact', 'ids', and 'canonical'
  * 
  * @return string 
  *   JSON representation of the entity with the specified format
  */
  function produceJSON($format = 'exact') {
     $json_array = array();
     if ($format == 'exact' || $format == 'canonical') {
      return $this->json;
     }
     elseif ($format == 'ids') {
      if ($this->object_type != 'Agent') {
        $json_array['objectType'] = $this->object_type;
      }
      $ifi_found = 0;
      if (!is_null($this->mbox) && $this->mbox != '') {
        $json_array['mbox'] = $this->mbox;
        $ifi_found = 1;
      }

      if (!$ifi_found && !is_null($this->mbox_sha1sum) && $this->mbox_sha1sum != '') {
        $json_array['mbox_sha1sum'] = $this->mbox_sha1sum;
        $ifi_found = 1;
      }

      if (!$ifi_found && !is_null($this->openid) && $this->openid != '') {
        $json_array['openid'] = $this->openid;
        $ifi_found = 1;
      }

      if (!$ifi_found && !is_null($this->account_home_page) && $this->account_home_page != '' && !is_null($this->account_name) && $this->account_name != '') {
        $json_array['account'] = array();
        $json_array['account']['homePage'] = $this->account_home_page;
        $json_array['account']['name'] = $this->account_name;
        $ifi_found = 1;
      }

      if ($json_array['objectType'] == 'Group') {
        $json_array['member'] = array();
        if (isset($this->tincan_group_members[LANGUAGE_NONE]) && count($this->tincan_group_members[LANGUAGE_NONE])) {
          foreach ($this->tincan_group_members[LANGUAGE_NONE] as $member) {
            $member_entity = entity_load_single('tincan_agent', $member['target_id']);
            $json_array['member'][] = $member_entity->produceJSON('ids');
          }
        }
      }

      $json = json_encode($json_array);
      $json = str_replace('\\/', '/', $json);
      $json = str_replace('\"', '"', $json);
      return $json;
    } // end if format == ids
  } // end produceJSON()
  
}

/**
 * The Controller for TincanAgent entities
 */
class TincanAgentController extends EntityAPIController {
  /**
   * {@inheritdoc}
   */
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Creates a TincanAgent entity 
   *
   * @return object TincanAgent
   *   A TincanAgent entity object with default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our tincan_agent
    $values += array( 
      'id' => '',
      'is_new' => TRUE,
    );
    $tincan_agent = parent::create($values);
    return $tincan_agent;
  }
 
  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    foreach ($entities as $id => $entity) {
      if (isset($entity->json)) {
        $entities[$id]->setJSON($entity->json);
      }
    }
    return $entities;
  }  

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'default', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity,$view_mode,$langcode,$content);
    return $build;
  }
}

/**
 * Provides TincanAgentMetadataController for TincanAgent entities
 */
class TincanAgentMetadataController extends EntityDefaultMetadataController {
  /**
   * Sets property metadata information for TincanAgent entities
   * @return array $info
   *   Array of TincanAgent property metadata information
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
      $info[$this->type]['properties']['id'] = array(
        'label' => t("Drupal Agent ID"), 
        'type' => 'integer', 
        'description' => t("The unique Drupal Agent ID."),
        'schema field' => 'id',
      );
      $info[$this->type]['properties']['object_type'] = array(
        'label' => t("Object Type"), 
        'type' => 'text', 
        'description' => t("Agent Object Type (either Agent or Group)"),
        'schema field' => 'object_type',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['name'] = array(
        'label' => t("Name"), 
        'type' => 'text', 
        'description' => t("Full name of the Agent"),
        'schema field' => 'name',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['mbox'] = array(
        'label' => t("Email IRI"), 
        'type' => 'text', 
        'description' => t("mailto IRI of the Agent"),
        'schema field' => 'mbox',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['mbox_sha1sum'] = array(
        'label' => t("mbox SHA1 hash"), 
        'type' => 'text', 
        'description' => t("The SHA1 hash of a mailto IRI"),
        'schema field' => 'mbox_sha1sum',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['openid'] = array(
        'label' => t("OpenID"), 
        'type' => 'text', 
        'description' => t("OpenID of the Agent"),
        'schema field' => 'openid',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['account_home_page'] = array(
        'label' => t("Account Homepage"), 
        'type' => 'text', 
        'description' => t("The canonical home page for the system the account is on."),
        'schema field' => 'account_home_page',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['account_name'] = array(
        'label' => t("Account UID"), 
        'type' => 'text', 
        'description' => t("The unique id or name used to log in to this account."),
        'schema field' => 'account_name',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['json'] = array(
        'label' => t("Agent JSON"), 
        'type' => 'text', 
        'description' => t("The JSON representation of the agent"),
        'schema field' => 'json',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
    return $info;
  }
}

/**
 * Provides TincanAgentUIController for TincanAgent entity
 */
class TincanAgentUIController extends EntityContentUIController {
  /**
   * Implements hook_menu()
   */
   public function hook_menu() {
      $items['tincan-agents/' . '%'] = array(
        'page callback' => 'tincan_lrs_agent_view',
        'page arguments' => array(1),
        'access callback' => 'tincan_lrs_tincan_agent_access',
        'file' => 'Agent.php',
        'file path' => drupal_get_path('module','tincan_lrs') . '/includes',
       // 'type' => MENU_CALLBACK,
      );
     return $items;
  }
  
}

/**
 * Provides TincanAgentExtraFieldsController for TincanAgent entity
 */
class TincanAgentExtraFieldsController extends EntityDefaultExtraFieldsController {
  protected $propertyInfo;

  /**
   * Implements EntityExtraFieldsControllerInterface::fieldExtraFields().
   */
  public function fieldExtraFields() {
    $extra = array();
    $this->propertyInfo = entity_get_property_info($this->entityType);
    if (isset($this->propertyInfo['properties'])) {
      foreach ($this->propertyInfo['properties'] as $name => $property_info) {
        // Skip adding the ID or bundle.
        if ($this->entityInfo['entity keys']['id'] == $name || $this->entityInfo['entity keys']['bundle'] == $name) {
          continue;
        }
        $extra[$this->entityType][$this->entityType]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
      }
    }
    // Handle bundle properties.
    $this->propertyInfo += array('bundles' => array());
    if (isset($this->propertyInfo['bundles'])) {
      foreach ($this->propertyInfo['bundles'] as $bundle_name => $info) {
        foreach ($info['properties'] as $name => $property_info) {
          if (empty($property_info['field'])) {
            $extra[$this->entityType][$bundle_name]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
          }
        }
      }
    }
    return $extra;
  }
}

/**
 * Provides TincanAgentDefaultViewsController for TincanAgent entities
 */
class TincanAgentDefaultViewsController extends EntityDefaultViewsController {
  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();
    return $data;
  }
  /**
   * {@inheritdoc}
   */
  function schema_fields() {
    $data = parent::schema_fields();
    return $data;
  }
  /**
   * {@inheritdoc}
   */
  function map_from_schema_info($property_name, $schema_field_info, $property_info) {
    $return = parent::map_from_schema_info($property_name, $schema_field_info, $property_info);
    return $return;
  }
}

/**
 * Loads UI controller and generates view pages for Tincan Agents
 *
 * Callback for hook_menu().
 *
 * @param integer id
 *
 * @return array $content
 *
 * @ingroup callbacks
 */
function tincan_lrs_agent_view($id) {
  $content = "";
  $entity = entity_load('tincan_agent', array($id));
  if (!empty($entity)) {
    $controller = entity_get_controller('tincan_agent');
    $content = $controller->view($entity);
    drupal_set_title($entity[$id]->label());
  }
  else {
    drupal_set_title('Agent not found.');
    $content = '<p>No agent found for drupal id: ' . $id . '</p>';
  }
  return $content;
}
