<?php
/**
 * @file
 * Defines TincanStatementAttachment entity object as well as all the associated controller objects 
 */

/**
 * Represents a tincan statement attachment
 */
class TincanStatementAttachment extends Entity {
  private $notation;
  
 /**
  * Populates the entity properties and fields from a decoded JSON array
  */ 
  private function populate() {
    $json = $this->notation;
    $json_array = drupal_json_decode($json);
        
    // usageType
    if (isset($json_array['usageType'])) {
      $this->usage_type = $json_array['usageType'];
    }
 
    // Name 
    if (isset($json_array['display'])) {
      // full name language map as json
      $this->display_json = drupal_json_encode($json_array['display']);
      // US English name
      if (isset($json_array['display']['en-US'])) {
        $this->name_en_us = $json_array['display']['en-US'];
      }
    }
    
    // Description
    if (isset($json_array['description'])) {
      // full description language map as json
      $this->description_json = drupal_json_encode($json_array['description']);
      // US English description
      if (isset($json_array['description']['en-US'])) {
        $this->description_en_us = $json_array['description']['en-US'];
      }
    }
      
    // contentType
    if (isset($json_array['contentType'])) {
      $this->content_type = $json_array['contentType'];
    }
      
    // length
    if (isset($json_array['length']) && is_numeric($json_array['length']) && $json_array['length'] > 0) {
      $this->length = $json_array['length'];
    }
    
    // sha2
    if (isset($json_array['sha2'])) {
        $this->sha2 = $json_array['sha2'];
    }
      
    // fileUrl
    // should this be set to the final location of the drupal file field tincan_statement_attachment 
    // if there is an actual file transmitted?
    if (isset($json_array['fileUrl'])) {
        $this->file_url = $json_array['fileUrl'];
    }
    
    // the actual file
    // maybe this isn't handled in the populate method, but if it is it should go here....
    
  } //end populate() method
  
 /**
  * Returns the default label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */  
  protected function defaultLabel() {
    if (isset($this->name_en_us) && $this->name_en_us != '') {
      return $this->name_en_us;
    }
    else {
      return $this->id;
    }
  }
  
 /**
  * Returns the default URI for the entity
  *
  * @return string
  *   Returns the URI for the entity
  */
  protected function defaultUri() {
    return array('path' => 'tincan-statement-attachment/' . $this->id);
  }
  
 /**
  * Constructs a TincanStatementAttachment entity
  */  
  public function __construct($values = array()) {
    parent::__construct($values, 'tincan_statement_attachment');
    $this->notation = isset($values['json']) ? $values['json'] : '';
  }

 /**
  * Returns the label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */      
  function label() {
    return $this->defaultLabel();
  }
  
 /**
  * Provides an array of the decoded JSON for the entity, from the received statement
  *
  * @return string
  *   Returns array of Decoded JSON of the entity
  */  
  function toArray() {
     return drupal_json_decode($this->notation);
  }

 /**
  * Provides received statement JSON for the entity
  *
  * @return string
  *   Returns the label for the entity
  */  
  function getJSON() {
    return $this->notation;
  }
 
 /**
  * Sets the JSON property for the entity
  */   
  function setJSON($json) {
    $this->notation = $json;
  }

 /**
  * Validates the JSON for the entity
  *
  * @return Boolean
  *   Returns TRUE if the JSON validates, otherwise FALSE
  */   
  function validateJSON() {
    if ($this->notation == '') {
      return FALSE;
    }
    return _tincan_lrs_basic_json_validation($this->notation, 'tincan_statement_attachment entity validation');
  }

 /**
  * Populates the properties and fields for the entity from the decoded entity JSON
  */
  function populateEntityValues() {
    if ($this->notation == '') {
      return FALSE;
    }
    if ($this->validateJSON()) {
      //process and populate entity
      $this->populate();
    }
  }

 /**
  * Produces JSON for the TincanStatementAttachment entity for the specified format
  *
  * @param string $format
  *   String specifing the format of JSON to produce
  *   Accepted values of 'exact', 'ids', and 'canonical'
  * 
  * @return string 
  *   JSON representation of the entity with the specified format
  */    
  function produceJSON($format = 'exact') {
    // its doubtful this method is necessary but we're including it for now. 
    $json_array = array();
    if($format == 'exact') {
      return $this->json;
    }
    elseif($format == 'canonical') {
      return $this->json; 
    }
    elseif($format == 'ids') {
      return $this->json;
    } // end if format == ids
  } // end produceJSON()
}

/**
 * Provides TincanStatementAttachmentController for TincanStatementAttachment entities
 */
class TincanStatementAttachmentController extends EntityAPIController {
  /**
   * {@inheritdoc}
   */
  public function __construct($entityType) {
    parent::__construct($entityType);
  }
  
  /**
   * Creates a TincanStatementAttachment entity 
   *
   * @return object TincanStatementAttachment
   *   A TincanStatementAttachment entity object with default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our tincan_statement
    $values += array( 
      'id' => '',
      'is_new' => TRUE,
    );
    
    $tincan_statement_attachment = parent::create($values);
    return $tincan_statement_attachment;
  }
 
  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    // Set json notation for the entities
    foreach ($entities as $id => $entity) {
      if (isset($entity->json)) {
        $entities[$id]->setJSON($entity->json);
      }
    }
    return $entities;
  }  
  
  /**
   * {@inheritdoc}
   */  
  public function buildContent($entity, $view_mode = 'default', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity,$view_mode,$langcode,$content);
    return $build;
  }
}

/**
 * Provides TincanStatementAttachmentMetadataController for TincanStatement entities
 */
class TincanStatementAttachmentMetadataController extends EntityDefaultMetadataController {
  /**
   * Sets property metadata information for TincanStatementAttachment entities
   * @return array $info
   *   Array of TincanStatementAttachment property metadata information
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
      $info[$this->type]['properties']['id'] = array(
        'label' => t("Drupal Statement Attachment ID"), 
        'type' => 'integer', 
        'description' => t("The unique Drupal Statement Attachment ID."),
        'schema field' => 'id',
      );
      $info[$this->type]['properties']['usage_type'] = array(
        'label' => t("Usage Type"), 
        'type' => 'text', 
        'description' => t("Identifies the usage of the attachment."),
        'schema field' => 'usage_type',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['name_en_us'] = array(
        'label' => t("Name"), 
        'type' => 'text', 
        'description' => t("American English name"),
        'schema field' => 'name_en_us',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['description_en_us'] = array(
        'label' => t("Description"), 
        'type' => 'text', 
        'description' => t("American English description"),
        'schema field' => 'description_en_us',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['content_type'] = array(
        'label' => t("Content Type"), 
        'type' => 'text', 
        'description' => t("HTTP Content Type of the attachment."),
        'schema field' => 'content_type',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['length'] = array(
        'label' => t("Length"), 
        'type' => 'integer', 
        'description' => t("The length of the attachment data in octets."),
        'schema field' => 'length',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['sha2'] = array(
        'label' => t("SHA2"), 
        'type' => 'text', 
        'description' => t("The SHA-2 (SHA-256, SHA-384, SHA-512) hash of the attachment data."),
        'schema field' => 'sha2',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['file_url'] = array(
        'label' => t("File URL"), 
        'type' => 'text', 
        'description' => t("An IRL at which the attachment data may be retrieved, or from which it used to be retrievable."),
        'schema field' => 'file_url',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['display_json'] = array(
        'label' => t("Display Name Language Map"), 
        'type' => 'text', 
        'description' => t("The Display Name JSON language map for this statement attachment."),
        'schema field' => 'display_json',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['description_json'] = array(
        'label' => t("Description Language Map"), 
        'type' => 'text', 
        'description' => t("The Description JSON language map for this statement attachment."),
        'schema field' => 'description_json',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['json'] = array(
        'label' => t("Attachment JSON"), 
        'type' => 'text', 
        'description' => t("The JSON representation of the statement attachment"),
        'schema field' => 'json',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
 
    return $info;
  }
}

/**
 * Provides TincanStatementAttachmentUIController for TincanStatementAttachment entity
 */
class TincanStatementAttachmentUIController extends EntityContentUIController {
  /**
   * Implements hook_menu()
   */
   public function hook_menu() {
      $items['tincan-statement-attachments/' . '%'] = array(
        'page callback' => 'tincan_lrs_statement_attachment_view',
        'page arguments' => array(1),
        'access callback' => 'tincan_lrs_tincan_statement_attachment_access',
        'file' => 'StatementAttachment.php',
        'file path' => drupal_get_path('module','tincan_lrs') . '/includes',
       // 'type' => MENU_CALLBACK,
      );
     return $items;
  }
  
}

/**
 * Provides TincanStatementAttachmentExtraFieldsController for TincanStatementAttachment entity
 */
class TincanStatementAttachmentExtraFieldsController extends EntityDefaultExtraFieldsController {
  protected $propertyInfo;

  /**
   * Implements EntityExtraFieldsControllerInterface::fieldExtraFields().
   */
  public function fieldExtraFields() {
    $extra = array();
    $this->propertyInfo = entity_get_property_info($this->entityType);
    if (isset($this->propertyInfo['properties'])) {
      foreach ($this->propertyInfo['properties'] as $name => $property_info) {
        // Skip adding the ID or bundle.
        if ($this->entityInfo['entity keys']['id'] == $name || $this->entityInfo['entity keys']['bundle'] == $name) {
          continue;
        }
        $extra[$this->entityType][$this->entityType]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
      }
    }
    // Handle bundle properties.
    $this->propertyInfo += array('bundles' => array());
    if (isset($this->propertyInfo['bundles'])) {
      foreach ($this->propertyInfo['bundles'] as $bundle_name => $info) {
        foreach ($info['properties'] as $name => $property_info) {
          if (empty($property_info['field'])) {
            $extra[$this->entityType][$bundle_name]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
          }
        }
      }
    }
    return $extra;
  }
}

/**
 * Provides TincanStatementAttachmentDefaultViewsController for TincanStatementAttachment entities
 */
class TincanStatementAttachmentDefaultViewsController extends EntityDefaultViewsController {
  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();

    return $data;
  }

  /**
   * {@inheritdoc}
   */  
  function schema_fields() {
    $data = parent::schema_fields();
    
    return $data;
  }

  /**
   * {@inheritdoc}
   */  
  function map_from_schema_info($property_name, $schema_field_info, $property_info) {
    $return = parent::map_from_schema_info($property_name, $schema_field_info, $property_info);
    
    return $return;
  }
  
}

/**
 * Loads UI controller and generates view pages for Tincan Statement Attachments
 *
 * Callback for hook_menu().
 *
 * @param integer id
 *
 * @return array $content
 *
 * @ingroup callbacks
 */
function tincan_lrs_statement_attachment_view($id) {
  $content = "";
  $entity = entity_load('tincan_statement_attachment', array($id));
  if (!empty($entity)) {
    $controller = entity_get_controller('tincan_statement_attachment');
    $content = $controller->view($entity);
    drupal_set_title($entity[$id]->label());
  }
  else {
    drupal_set_title('Statement attachment not found.');
    $content = '<p>No statement attachment found for drupal id: ' . $id . '</p>';
  }
  return $content;
}
