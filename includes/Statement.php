<?php
/**
 * @file
 * Defines TincanStatement entity object as well as all the associated controller objects 
 */

/**
 * Represents a tincan statement
 */
class TincanStatement extends Entity {
  private $notation;
  private $array;

  /**
   * Finds an agent entity
   *
   * @param array $json_array
   *   Array of values parsed from JSON
   *
   * @return integer
   *   Returns the entity id of the agent if found, otherwise 0;
   */
  private function findAgent($json_array) {
    if (!isset($json_array['mbox']) &&
       !isset($json_array['mbox_sha1sum']) &&
       !isset($json_array['openid']) &&
       (!isset($json_array['account']) && !isset($json_array['account']['homePage']) && ! isset($json_array['account']['name'])) ) {
      return 0;  
    }

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type','tincan_agent');
    if (isset($json_array['objectType'])) {
      switch ($json_array['objectType']) {
        case 'Agent':
          $query->propertyCondition('object_type','Agent');
          break;
        case 'Group':
          $query->propertyCondition('object_type','Group');
          break;
      }
    }
    else {
      $query->propertyCondition('object_type','Agent');
    }
    $ifi_found = 0;
    if (isset($json_array['mbox'])) {
      $query->propertyCondition('mbox',$json_array['mbox']);
      $ifi_found = 1;
    }
    if (isset($json_array['mbox_sha1sum']) && !$ifi_found) {
      $query->propertyCondition('mbox_sha1sum',$json_array['mbox_sha1sum']);
      $ifi_found = 1;
    }
    if (isset($json_array['openid']) && !$ifi_found) {
      $query->propertyCondition('openid',$json_array['openid']);
      $ifi_found = 1;
    }
    if (isset($json_array['account']) && isset($json_array['account']['homePage']) && isset($json_array['account']['name']) && !$ifi_found) {
      $query->propertyCondition('account_home_page',$json_array['account']['homePage']);
      $query->propertyCondition('account_name',$json_array['account']['name']);
      $ifi_found = 1;
    }
    $result = $query->execute();

    if (isset($result['tincan_agent'])) {
      foreach ($result['tincan_agent'] as $key => $agent) {
        return $key;
      }
    }
    else return 0;
  }

  /**
   * Finds an activity entity
   *
   * @param array $json_array
   *   Array of values parsed from JSON
   *
   * @return integer
   *   Returns the entity id of the activity if found, otherwise 0;
   */
  private function findActivity($json_array) {

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type','tincan_activity');
    if (isset($json_array['id'])) {
      $query->propertyCondition('activity_id', $json_array['id']);
    }

    $result = $query->execute();

    if (isset($result['tincan_activity'])) {
      foreach ($result['tincan_activity'] as $key => $activity) {
        return $key;
      }
    }
    else return 0;
  }

 /**
  * Creates an agent entity
  *
  * @param array $json_array
  *   Array of values parsed from JSON
  *
  * @return integer
  *   Returns the entity id of the agent if created successfully, otherwise 0;
  */  
  private function createAgent($json_array) {
    $values = array();
    $target_id = 0;
    $json = drupal_json_encode($json_array);
    $values['json'] = $json;

    $tincan_agent_entity = tincan_lrs_agent_create($values);
    $tincan_agent_entity->populateEntityValues();
    try {
      $save_result = $tincan_agent_entity->save();
    }
    catch (Exception $e) {
      services_error ('Internal Server Error', 500, 'Statement create agent save fail: ' . $e->getMessage());
    }
    if ($save_result) {
      $target_id = $tincan_agent_entity->id;
    }

    return $target_id;
  }

 /**
  * Creates an statement attachment entity
  *
  * @param array $json_array
  *   Array of values parsed from JSON
  *
  * @return integer
  *   Returns the entity id of the statement attachment if created successfully, otherwise 0;
  */   
  private function createAttachment($json_array) {
    // this method only handles the case of attachments with a fileUrl, 
    // and not with an actual binary file sent with the request
    // that is on the todo list
    $values = array();
    $target_id = 0;
    $json = drupal_json_encode($json_array);
    $values['json'] = $json;

    $tincan_attachment_entity = tincan_lrs_statement_attachment_create($values);
    $tincan_attachment_entity->populateEntityValues();
    try {
      $save_result = $tincan_attachment_entity->save();
    }
    catch (Exception $e) {
      services_error ('Internal Server Error', 500, 'Statement attachment create save fail: ' . $e->getMessage());
    }
    if ($save_result) {
      $target_id = $tincan_attachment_entity->id;
    }

    return $target_id;
  }

 /**
  * Strips non-essential values from an JSON decoded array of an Agent
  *
  * @param array $json_array
  *   Array of values parsed from JSON
  *
  * @return array
  *   Returns an array of values, which is the minimum values to describe a Agent for the "ids" format of the Statement GET results 
  */ 
  private function idFormatStripAgent($json_array) {
    //unset objectType if it is Agent
    if (isset($json_array['objectType']) && $json_array['objectType'] == 'Agent') {
      unset($json_array['objectType']);  
    }
    //unset name
    if (isset($json_array['name'])) {
      unset($json_array['name']);    
    }
    //unset unnecessary data from each member Agent array
    if (isset($json_array['member'])) {
      foreach ($json_array['member'] as $index => $member_array) {
        $json_array['member'][$index] = $this->idFormatStripAgent($member_array);        
      }
    }
    return $json_array;
  } // end method idFormatStripAgent()

 /**
  * Creates an activity entity
  *
  * @param array $json_array
  *   Array of values parsed from JSON
  *
  * @return integer
  *   Returns the entity id of the activity if created successfully, otherwise 0;
  */ 
  private function createActivity($json_array) {
    $values = array();
    $target_id = 0;

    $json = drupal_json_encode($json_array);
    $values['json'] = $json;
          
    $tincan_activity_entity = tincan_lrs_activity_create($values);
    $tincan_activity_entity->populateEntityValues();
    try {
      $save_result = $tincan_activity_entity->save();
    }
    catch (Exception $e) {
      services_error ('Internal Server Error', 500, 'Statement create activity save fail: ' . $e->getMessage());
    }
    if ($save_result) {
      $target_id = $tincan_activity_entity->id;
    }

    return $target_id;
  }

 /**
  * Populates the entity properties and fields from set entity JSON
  */ 
  private function populate() {
    $json = $this->notation;
    $json_array = drupal_json_decode($json);

    // object_type
    if (isset($json_array['objectType']) && $json_array['objectType']=='SubStatement') {
      $this->object_type = 'SubStatement';
      if (isset($json_array['version'])) {
        unset($json_array['version']);
      }
      if (isset($json_array['id'])) {
        unset($json_array['id']);
      }
      if (isset($json_array['stored'])) {
        unset($json_array['stored']);
      }
      if (isset($json_array['authority'])) {
        unset($json_array['authority']);
      }
    }
    else {
      $this->object_type = 'Statement';
    }
    // version
    if(isset($json_array['version'])) {
      $this->version = $json_array['version'];
    }
    else {
      if($this->object_type != 'SubStatement') {
        $this->version = '1.0.0';
      }
    }
    // id
    if(isset($json_array['id'])) {
      $this->statement_id = $json_array['id'];
    }
    else { // generate uuid for the statement
      if($this->object_type != 'SubStatement' && !isset($this->statement_id)) {
        $this->statement_id = uuid_generate();
      }
    }

    // timestamp
    if(isset($json_array['timestamp'])) {
      $this->timestamp = $json_array['timestamp'];
    }
    // stored
    if(isset($json_array['stored'])) {
      // $this->stored = $json_array['stored'];
      // thinking the LRS should always set the stored value
      if($this->object_type != 'SubStatement') {
        $this->stored_date = date('c'); //must be in iso 8601 format;
      }
    }
    else {
      if($this->object_type != 'SubStatement') {
        $this->stored_date = date('c'); //must be in iso 8601 format;
      }
    }

    // Actor
    if(isset($json_array['actor'])) {
      $this->tincan_actor = array();
      $this->populateActor($json_array['actor']);
    }
    // Verb
    if(isset($json_array['verb'])) {
      $this->tincan_verb = array();
      $this->populateVerb($json_array['verb']);
    }

    // Object
    if(isset($json_array['object'])) {
      $this->tincan_object = array();
      $this->populateObject($json_array['object']);
    }
    // Result
    if (isset($json_array['result'])) {
      $this->tincan_result = array();
      $this->populateResult($json_array['result']);
    }
    // Authority
    // https://github.com/adlnet/xAPI-Spec/blob/master/xAPI.md#authority
    // "The LRS SHOULD overwrite the authority on all stored received Statements, based on the credentials used to send those Statements."
    // if there is already an authority, we should overwrite it if the user is not anonymous that was used to access the api
    // the rewriting of the authority will cause a difference between the sent statement, and the stored statement.
    if (!user_is_anonymous() && $this->object_type != 'SubStatement') {
      global $user;
      $authority = array();
      $authority['objectType'] = 'Agent';
      $authority['name'] = $user->name;
      $authority['mbox'] = $user->mail;
      $json_array['authority'] = $authority;
    }

    if (isset($json_array['authority']) ) {
      $this->populateAuthority($json_array['authority']);
    }

    if (isset($json_array['context'])) {
      $this->tincan_context = array();
      $this->populateContext($json_array['context']);
    }

    if (isset($json_array['attachments'])) {
      $this->tincan_statement_attachment = array();
      $this->populateAttachments($json_array['attachments']);
    }
  } //end of populate method

 /**
  * Populates the tincan_actor field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the actor object
  */ 
  private function populateActor($json_array) {
    $target_id = $this->findAgent($json_array);

    if (!$target_id) {
      $target_id = $this->createAgent($json_array);
    }
    if ($target_id) {
      $this->tincan_actor[LANGUAGE_NONE][0]['target_id'] = $target_id;
    }
  }

 /**
  * Populates the tincan_verb field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the verb object
  */
  private function populateVerb($json_array) {
    if (isset($json_array['id'])) {
       $this->tincan_verb[LANGUAGE_NONE][0]['id'] = $json_array['id'];
    }
    if (isset($json_array['display']['en-US'])) { 
      $this->tincan_verb[LANGUAGE_NONE][0]['display_en_us'] = $json_array['display']['en-US'];
    }
    if (isset($json_array['display'])) {
      $this->tincan_verb[LANGUAGE_NONE][0]['display'] = drupal_json_encode($json_array['display']);
    }

    $this->tincan_verb[LANGUAGE_NONE][0]['json'] = drupal_json_encode($json_array);
  }

 /**
  * Populates the tincan_object field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the 'object' object
  */
  private function populateObject($json_array) {
    $target_id = 0;
    if (isset($json_array['objectType'])) {
       $this->tincan_object[LANGUAGE_NONE][0]['object_type'] = $json_array['objectType'];
       switch ($json_array['objectType']) {
         case 'Activity':
           $this->tincan_object[LANGUAGE_NONE][0]['table'] = 'tincan_activity';
           $target_id = $this->findActivity($json_array);
           if (!$target_id) {
             $target_id = $this->createActivity($json_array);
           }
           break;
         case 'Agent':
         case 'Group':
           $this->tincan_object[LANGUAGE_NONE][0]['table'] = 'tincan_agent';
           $target_id = $this->findAgent($json_array);
           if (!$target_id) {
             $target_id = $this->createAgent($json_array);
           } 
           break;
         case 'SubStatement':
           $this->tincan_object[LANGUAGE_NONE][0]['table'] = 'tincan_statement';
           $substatement_json = drupal_json_encode($json_array);
           $tincan_substatement_entity = tincan_lrs_statement_create(array('json' => $substatement_json));
           $tincan_substatement_entity->populateEntityValues();
           try {
             $save_result = $tincan_substatement_entity->save();
           }
           catch (Exception $e) {
             services_error ('Internal Server Error', 500, 'Statement create sub-statement create save fail: ' . $e->getMessage());
           }
           if ($save_result) {
             $target_id = $tincan_substatement_entity->id;
           }
           break;
         case 'StatementRef':
           $this->tincan_object[LANGUAGE_NONE][0]['table'] = 'statement_reference';
           if (isset($json_array['id'])) {
             $target_id = $json_array['id'];
           }
           break;
       }
    }
    else {
      $target_id = $this->findActivity($json_array);
      if (!$target_id) {
        $target_id = $this->createActivity($json_array);
      }
      $this->tincan_object[LANGUAGE_NONE][0]['object_type'] = 'Activity';
      $this->tincan_object[LANGUAGE_NONE][0]['table'] = 'tincan_activity';
    }

    if ($target_id) {
      $this->tincan_object[LANGUAGE_NONE][0]['target_id'] = $target_id;
    }
    $this->tincan_object[LANGUAGE_NONE][0]['json'] = drupal_json_encode($json_array);
  }

 /**
  * Populates the tincan_result field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the result object
  */
  private function populateResult($json_array) {
    if (isset($json_array['score'])) {
      $this->tincan_result[LANGUAGE_NONE][0]['score_json'] = drupal_json_encode($json_array['score']);

      if (isset($json_array['score']['scaled']) && is_numeric($json_array['score']['scaled'])) {
        $this->tincan_result[LANGUAGE_NONE][0]['score_scaled'] = $json_array['score']['scaled'];
      }
      if (isset($json_array['score']['raw']) && is_numeric($json_array['score']['raw'])) {
        $this->tincan_result[LANGUAGE_NONE][0]['score_raw'] = $json_array['score']['raw'];
      }
      if (isset($json_array['score']['min']) && is_numeric($json_array['score']['min'])) {
        $this->tincan_result[LANGUAGE_NONE][0]['score_min'] = $json_array['score']['min'];
      }
      if (isset($json_array['score']['max']) && is_numeric($json_array['score']['max'])) {
        $this->tincan_result[LANGUAGE_NONE][0]['score_max'] = $json_array['score']['max'];
      }
    }
    if (isset($json_array['success'])) {
      $this->tincan_result[LANGUAGE_NONE][0]['success'] = ($json_array['success'] == 1 || $json_array['success'] == 'true') ? 1 : 0;
    }

    if (isset($json_array['completion'])) {
      $this->tincan_result[LANGUAGE_NONE][0]['completion'] = ($json_array['completion'] == 1 || $json_array['completion'] == 'true') ? 1 : 0;
    }
    if (isset($json_array['response'])) {
      $this->tincan_result[LANGUAGE_NONE][0]['response'] = $json_array['response'];
    }
    if (isset($json_array['duration'])) {
      $this->tincan_result[LANGUAGE_NONE][0]['duration'] = $json_array['duration'];
    }
    if (isset($json_array['extensions'])) {
      $this->tincan_result[LANGUAGE_NONE][0]['extensions'] = drupal_json_encode($json_array['extensions']);
    }

    $this->tincan_result[LANGUAGE_NONE][0]['json'] = drupal_json_encode($json_array);
  }

 /**
  * Populates the tincan_authority field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the authority object
  */
  private function populateAuthority($json_array) {
    $target_id = $this->findAgent($json_array);

    if(!$target_id) {
      $target_id = $this->createAgent($json_array);
    }
    if($target_id) {
      $this->tincan_authority[LANGUAGE_NONE][0]['target_id'] = $target_id;
    }
  }

 /**
  * Populates the tincan_context field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the context object
  */
  private function populateContext($json_array) {

    if (isset($json_array['registration'])) {
      $this->tincan_context[LANGUAGE_NONE][0]['registration'] = $json_array['registration'];
    }

    if (isset($json_array['revision'])) {
      $this->tincan_context[LANGUAGE_NONE][0]['revision'] = $json_array['revision'];
    }

    if (isset($json_array['platform'])) {
      $this->tincan_context[LANGUAGE_NONE][0]['platform'] = $json_array['platform'];
    }

    if (isset($json_array['language'])) {
      $this->tincan_context[LANGUAGE_NONE][0]['language'] = $json_array['language'];
    }

    if (isset($json_array['statement'])) {
      if (isset($json_array['statement']['objectType'])) {
        $this->tincan_context[LANGUAGE_NONE][0]['statement_reference_object_type'] = $json_array['statement']['objectType'];
      }
      if (isset($json_array['statement']['id'])) {
        $this->tincan_context[LANGUAGE_NONE][0]['statement_reference_statement_id'] = $json_array['statement']['id'];
      }
    }

    if (isset($json_array['instructor'])) {
      $target_id = $this->findAgent($json_array);

      if (!$target_id) {
        $target_id = $this->createAgent($json_array);
      }
      if ($target_id) {
        $this->tincan_instructor[LANGUAGE_NONE][0]['target_id'] = $target_id;
      }
    }

    if (isset($json_array['team'])) {
      $target_id = $this->findAgent($json_array);
    
      if (!$target_id) {
        $target_id = $this->createAgent($json_array);
      }
      if ($target_id) {
        $this->tincan_team[LANGUAGE_NONE][0]['target_id'] = $target_id;
      }
    }

    if (isset($json_array['contextActivities'])) {
      $this->tincan_context[LANGUAGE_NONE][0]['context_activities_json'] = drupal_json_encode($json_array['contextActivities']);
      $context_activities_types = array('parent', 'grouping', 'category', 'other');
      foreach ($context_activities_types as $type) {
        if (isset($json_array['contextActivities'][$type]) && is_array($json_array['contextActivities'][$type])) {

          if (!isset($json_array['contextActivities'][$type][0]) ) {
            $target_id = $this->findActivity($json_array['contextActivities'][$type]);
            if (!$target_id) {
              $target_id = $this->createActivity($json_array['contextActivities'][$type]);
            }
            if ($target_id) {
              $this->{'tincan_context_activity_' . $type}[LANGUAGE_NONE][0]['target_id'] = $target_id;
            }
          }
          else {
            $count  = 0;
            foreach ($json_array['contextActivities'][$type] as $item) {

              $target_id = $this->findActivity($item);
              if (!$target_id) {
                $target_id = $this->createActivity($item);
              }
              if ($target_id) {
                $this->{'tincan_context_activity_' . $type}[LANGUAGE_NONE][$count]['target_id'] = $target_id;
              }
              $count += 1;
            }
          }
        }
      }
    }

    if (isset($json_array['extensions'])) {
      $this->tincan_context[LANGUAGE_NONE][0]['extensions'] = drupal_json_encode($json_array['extensions']);
    }

    // need context_activities_json
    $this->tincan_context[LANGUAGE_NONE][0]['json'] = drupal_json_encode($json_array);
  }

 /**
  * Populates the tincan_statement_attachement field from a decoded JSON array
  *
  * @param array $json_array
  *   Array of decoded JSON values for the attachments object
  */
  private function populateAttachments($json_array) {
    foreach ($json_array as $index => $attachment) {
      $target_id = $this->createAttachment($attachment);
      if ($target_id) {
        $this->tincan_statement_attachment[LANGUAGE_NONE][$index]['target_id'] = $target_id;
      }
    }
  }

 /**
  * Produces JSON for the TincanStatement entity in the specified format
  *
  * @param string $format
  *   String specifing the format of JSON to produce
  *   Accepted values of 'exact', 'ids', and 'canonical'
  * 
  * @return string 
  *   JSON representation of the entity with the specified format
  */
  function produceJSON($format = 'exact') {
    // to make the produced json exactly like it was received, we're starting with the original json
    // and taking away elements when necessary for the ids and canonical values for format
    // this is due to default objectTypes being set in the data when stored, but not necessarily being in the consumed json
    // example, for actor, objectType = Agent is a default and not always explicitly set
    $json_array = drupal_json_decode($this->json);
    // add the version, stored, and authority values that are set by the LRS on statement create
    $json_array['version'] = $this->version;
    $json_array['stored'] = $this->stored_date;
    // authority
    $authority_agent_id = $this->tincan_authority[LANGUAGE_NONE][0]['target_id'];
    $authority_agent_entity = entity_load_single('tincan_agent', $authority_agent_id);
    $json_array['authority'] = drupal_json_decode($authority_agent_entity->produceJSON($format));

    if ($format == 'exact') {
      // do nothing, the updated $this->json will be returned at end of function
    }
    elseif ($format == 'canonical') {
      // here we need to follow the spec for returning data for the name and description based
      // on the received Accept Language header
      // Section 14.4 here: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
      $accept_language_header = _tincan_lrs_get_header_value('Accept-Language');

      // I'm really thinking that it would be best to do this area once we figure out a better handling of the Language Map objects
      // this code would change when we do better Drupal multi-lingual integration    


      // so for now, just returning the entire json
    }
    elseif ($format == 'ids') {
      // actor
      if (isset($json_array['actor'])) {
        $json_array['actor'] = $this->idFormatStripAgent($json_array['actor']);
      }

      // object
      if (isset($json_array['object']) && ($json_array['object']['objectType'] == 'Agent' ||  $json_array['object']['objectType'] == 'Group')) {
        $json_array['object'] = $this->idFormatStripAgent($json_array['object']);
      }
      elseif (isset($json_array['object']) && $json_array['object']['objectType'] == 'Activity') {
        foreach ($json_array['object'] as $key => $value) {
          if ($key != 'id') {
            unset($json_array['object'][$key]);
          }
        }
      }

      // authority
      $json_array['authority'] = $this->idFormatStripAgent($json_array['authority']);

      // context team
      if (isset($json_array['context']['team'])) {
        $json_array['context']['team'] = $this->idFormatStripAgent($json_array['context']['team']);
      }

      // context instructor
      if (isset($json_array['context']['instructor'])) {
        $json_array['context']['instructor'] = $this->idFormatStripAgent($json_array['context']['instructor']);
      }

      // contextActivities
      if (isset($json_array['context']['contextActivities'])) {
       // parent
        foreach ($json_array['context']['contextActivities']['parent'] as $index => $activity) {
          foreach ($activity as $key => $value) {
            if ($key != 'id') {
              unset($json_array['contextActivities'][$index][$key]);
            }
          }
        }
        // grouping
        foreach($json_array['context']['contextActivities']['grouping'] as $index => $activity) {
          foreach($activity as $key => $value) {
            if($key != 'id') {
              unset($json_array['contextActivities'][$index][$key]);
            }
          }
        }
        // category
        foreach($json_array['context']['contextActivities']['category'] as $index => $activity) {
          foreach($activity as $key => $value) {
            if($key != 'id') {
              unset($json_array['contextActivities'][$index][$key]);
            }
          }
        }
        // other
        foreach($json_array['context']['contextActivities']['other'] as $index => $activity) {
          foreach($activity as $key => $value) {
            if($key != 'id') {
              unset($json_array['contextActivities'][$index][$key]);
            }
          }
        }
      } // end if isset contextActivities

    } // end if format = ids

    $json = json_encode($json_array);
    $json = str_replace('\\/', '/', $json);
    $json = str_replace('\"', '"', $json);
    return $json;
  } //end method produceJSON()

 /**
  * Returns the default label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */
  protected function defaultLabel() {
    if (isset($this->statement_id) && $this->statement_id) {
      return $this->statement_id;
    }
    else {
      return $this->id;
    }
  }

 /**
  * Returns the default URI for the entity
  *
  * @return string
  *   Returns the URI for the entity
  */
  protected function defaultUri() {
    return array('path' => 'tincan-statements/' . $this->id);
  }

 /**
  * Constructs a TincanStatement entity
  */
  public function __construct($values = array()) {
    parent::__construct($values, 'tincan_statement');
    $this->notation = isset($values['json']) ? $values['json'] : '';
  }

 /**
  * Returns the label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */  
  function label() {
    return $this->defaultLabel();
  }

 /**
  * Provides an array of the decoded JSON for the entity, from the received statement
  *
  * @return string
  *   Returns array of Decoded JSON of the entity
  */  
  function toArray() {
    return drupal_json_decode($this->notation);
  }

 /**
  * Provides received statement JSON for the entity
  *
  * @return string
  *   Returns the label for the entity
  */
  function getJSON() {
    return $this->notation;
  }

 /**
  * Sets the working JSON for the entity
  */  
  function setJSON($json) {
    $this->notation = $json;
  }

 /**
  * Validates the JSON for the entity
  *
  * @return Boolean
  *   Returns TRUE if the JSON validates, otherwise FALSE
  */   
  function validateJSON() {
    if(!isset($this->notation) || $this->notation == '') return FALSE;
    return _tincan_lrs_basic_json_validation($this->notation,'tincan_statement entity validation');
  }

 /**
  * Sets the voided property to 1 for the voiding statement's referenced statement
  *
  * @return string
  *   Returns a message regarding the voiding action attempted
  */    
  function voidReferencedStatement() {
    $json = $this->notation;
    $json_array = drupal_json_decode($json);

    if (isset($json_array['verb']['id']) && $json_array['verb']['id']=='http://adlnet.gov/expapi/verbs/voided') {
      if (isset($json_array['object']['objectType']) && $json_array['object']['objectType']=='StatementRef') {
        if (isset($json_array['object']['id']) ) {
          $statement_id_to_void = $json_array['object']['id'];
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'tincan_statement');
          $query->propertyCondition('statement_id', $statement_id_to_void);
            // Any Statement that voids another cannot itself be voided.
          $query->fieldCondition('tincan_verb', 'id', 'http://adlnet.gov/expapi/verbs/voided', '<>');
          $results = $query->execute();

          if (isset($results['tincan_statement']) && count($results['tincan_statement']) ) {
            foreach ($results['tincan_statement'] as $id => $array) {
              $entity = entity_load_single('tincan_statement', $id);
              $entity->voided = 1;
              try {
                $entity->save();
              }
              catch (Exception $e) {
                return 'save_error';
              }
              return 'void_success';
            }
          }
          else {
            return 'not_found';
          }
               
        } // end if object id is set
        else {
          return 'Void error. Object id not set.';
        }
      } // end if object type = StatementRef
      else {
        return 'Void error. Statement must have object with objectType = StatementRef to void another statement';
      }
    } // end if verb id = voided
    return 'Void error. Only statements with the verb id http://adlnet.gov/expapi/verbs/voided can void other statements.';
  }

 /**
  * Populates the properties and fields for the entity from the decoded entity JSON
  */
  function populateEntityValues() {
    if($this->notation == '') return FALSE;
    if($this->validateJSON()) {
      try {
        $this->populate();
      } 
      catch(Exception $e) {
        services_error ('Internal Server Error', 500,  $e->getMessage());
      }
    }
  }

} // end class TincanStatement

/**
 * Provides TincanStatementController for TincanStatement entities
 */
class TincanStatementController extends EntityAPIController {
  /**
   * {@inheritdoc}
   */
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Creates a TincanStatement entity 
   *
   * @return object TincanStatement
   *   A TincanStatement entity object with default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our tincan_statement
    $values += array( 
      'id' => '',
      'is_new' => TRUE,
      );
    
    $tincan_statement = parent::create($values);
    return $tincan_statement;
  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    // Set json notation for the entities    
    foreach ($entities as $id => $entity) {
      if (isset($entity->json)) {
        $entities[$id]->setJSON($entity->json);
      }
    }

    return $entities;
  }  

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'default', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity,$view_mode,$langcode,$content);
    return $build;
  }
}

/**
 * Provides TincanStatementMetadataController for TincanStatement entities
 */
class TincanStatementMetadataController extends EntityDefaultMetadataController {
  /**
   * Sets property metadata information for TincanStatement entities
   *
   * @return array $info
   *   Array of TincanStatement property metadata information
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
      $info[$this->type]['properties']['id'] = array(
        'label' => t("Drupal Statement ID"), 
        'type' => 'integer', 
        'description' => t("The unique Drupal statement ID."),
        'schema field' => 'id',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['statement_id'] = array(
        'label' => t("Statement ID"), 
        'type' => 'text', 
        'description' => t("The unique AP provided statement ID."),
        'schema field' => 'statement_id',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
  
      $info[$this->type]['properties']['stored_date'] = array(
        'label' => t("Statement Stored Date"), 
        'type' => 'date', 
        'description' => t("The date the statement was stored"),
        'schema field' => 'stored_date',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['timestamp'] = array(
        'label' => t("Statement Timestamp"), 
        'type' => 'date', 
        'description' => t("The date when the events described within the statement occurred."),
        'schema field' => 'timestamp',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['version'] = array(
        'label' => t("Statement Version"), 
        'type' => 'text', 
        'description' => t("The Version of the statement"),
        'schema field' => 'version',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['object_type'] = array(
        'label' => t("Statement Object Type"), 
        'type' => 'text', 
        'description' => t("Object Type of the statement (statement or substatement)"),
        'schema field' => 'object_type',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['voided'] = array(
        'label' => t("Voided"), 
        'type' => 'text', 
        'description' => t("Flag representing whether the statement as been voided"),
        'schema field' => 'voided',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['json'] = array(
        'label' => t("Statement JSON"), 
        'type' => 'text', 
        'description' => t("The JSON representation of the statement"),
        'schema field' => 'json',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
    return $info;
  }
}

/**
 * Provides TincanStatementUIController for TincanStatement entities
 */
class TincanStatementUIController extends EntityContentUIController {
  /**
   * Implements hook_menu()
   */
   public function hook_menu() {
      $items['tincan-statements/' . '%'] = array(
        'page callback' => 'tincan_lrs_statement_view',
        'page arguments' => array(1),
        'access callback' => 'tincan_lrs_tincan_statement_access',
        'file' => 'Statement.php',
        'file path' => drupal_get_path('module','tincan_lrs') . '/includes',
       // 'type' => MENU_CALLBACK,
      );
     return $items;
  }
}

/**
 * Provides TincanStatementDefaultViewsController for TincanStatement entities
 */
class TincanStatementDefaultViewsController extends EntityDefaultViewsController {
   /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    $data['tincan_statement']['stored_date'] = array(
      'title' => t('Stored'),
      'help' => t('Stored date value, the date the statement was stored'),
      'field' => array(
        'handler' => 'tincan_lrs_handler_field_datetime',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'tincan_lrs_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'tincan_lrs_handler_filter_datetime',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_tincanlrs_fulldate',
      ),
    );
    $data['tincan_statement']['timestamp'] = array(
      'title' => t('Timestamp'),
      'help' => t('Timestamp of the statement'),
      'field' => array(
        'handler' => 'tincan_lrs_handler_field_datetime',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'tincan_lrs_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'tincan_lrs_handler_filter_datetime',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_tincanlrs_fulldate',
      ),
    );

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  function schema_fields() {
    $data = parent::schema_fields();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  function map_from_schema_info($property_name, $schema_field_info, $property_info) {
    $return = parent::map_from_schema_info($property_name, $schema_field_info, $property_info);
    return $return;
  }
}

/**
 * Provides TincanStatementExtraFieldsController for TincanStatement entity
 */
class TincanStatementExtraFieldsController extends EntityDefaultExtraFieldsController {
  protected $propertyInfo;

  /**
   * Implements EntityExtraFieldsControllerInterface::fieldExtraFields().
   *
   * Overrided this function because it was producing notices
   */
  public function fieldExtraFields() {
    $extra = array();
    $this->propertyInfo = entity_get_property_info($this->entityType);
    if(isset($this->propertyInfo['properties'])) {
      foreach ($this->propertyInfo['properties'] as $name => $property_info) {
        // Skip adding the ID or bundle.
        if ($this->entityInfo['entity keys']['id'] == $name || $this->entityInfo['entity keys']['bundle'] == $name) {
          continue;
        }
        $extra[$this->entityType][$this->entityType]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
      }
    }
    // Handle bundle properties.
    $this->propertyInfo += array('bundles' => array());
    if(isset($this->propertyInfo['bundles'])) {
      foreach ($this->propertyInfo['bundles'] as $bundle_name => $info) {
        foreach ($info['properties'] as $name => $property_info) {
          if (empty($property_info['field'])) {
            $extra[$this->entityType][$bundle_name]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
          }
        }
      }
    }
    return $extra;
  }
}

/**
 * Loads UI controller and generates view pages for Tincan Statements
 *
 * Callback for hook_menu().
 *
 * @param integer id
 *
 * @return array $content
 *
 * @ingroup callbacks
 */
function tincan_lrs_statement_view($id) {
  $content = "";
  $entity = entity_load('tincan_statement', array($id));

  if (!empty($entity)) {
    $controller = entity_get_controller('tincan_statement');
    $content = $controller->view($entity);
    drupal_set_title($entity[$id]->label());
  }
  else {
    drupal_set_title('Statement not found.');
    $content = '<p>No statement found for drupal id: ' . $id . '</p>';
  }

  return $content;
}
