<?php
/**
 * @file
 * Defines TincanActivityProfile entity object as well as all the associated controller objects 
 */

/**
 * Represents a Tincan Activity Profile
 */
class TincanActivityProfile extends Entity {
 /**
  * Constructs a TincanActivityProfile entity
  */  
  public function __construct($values = array()) {
    parent::__construct($values, 'tincan_activity_profile');
  }

 /**
  * Returns the default label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */
	protected function defaultLabel() {
    return $this->profile_id;
  }

 /**
  * Returns the label for the entity
  *
  * @return string
  *   Returns the label for the entity
  */
  function label() {
    return $this->defaultLabel();
  }

 /**
  * Returns the default URI for the entity
  *
  * @return string
  *   Returns the URI for the entity
  */ 
  protected function defaultUri() {
    return array('path' => 'tincan-activity-profiles/' . $this->id);
  }
}

/**
 * Provides TincanActivityProfileController for TincanActivityProfile entities
 */
class TincanActivityProfileController extends EntityAPIController {
  /**
   * {@inheritdoc}
   */
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Creates a TincanActivityProfile entity 
   *
   * @return object TincanActivityProfile
   *   A TincanActivityProfile entity object with default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our tincan_activity_profile
    $values += array( 
      'id' => '',
      'is_new' => TRUE,
    );
    $tincan_activity_profile = parent::create($values);
    return $tincan_activity_profile;
  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    return $entities;
  }  

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'default', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity,$view_mode,$langcode,$content);
    return $build;
  }
}

/**
 * Provides TincanActivityProfileMetadataController for TincanActivityProfile entities
 */
class TincanActivityProfileMetadataController extends EntityDefaultMetadataController {
  /**
   * Sets property metadata information for TincanActivityProfile entities
   * @return array $info
   *   Array of TincanActivityProfile property metadata information
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
      $info[$this->type]['properties']['id'] = array(
        'label' => t("Drupal Activity Profile ID"), 
        'type' => 'integer', 
        'description' => t("The unique Drupal Activity Profile ID."),
        'schema field' => 'id',
      );
      $info[$this->type]['properties']['profile_id'] = array(
        'label' => t("Profile ID"), 
        'type' => 'text', 
        'description' => t("The unique activity profile id"),
        'schema field' => 'profile_id',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['activity_id'] = array(
        'label' => t("Activity ID"), 
        'type' => 'text', 
        'description' => t("Activity id providing context for this activity profile."),
        'schema field' => 'activity_id',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['stored_date'] = array(
        'label' => t("Stored"), 
        'type' => 'date', 
        'description' => t("The stored date"),
        'schema field' => 'stored_date',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['updated'] = array(
        'label' => t("Updated"), 
        'type' => 'date', 
        'description' => t("The updated date"),
        'schema field' => 'updated',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['content_type'] = array(
        'label' => t("HTTP Content Type"), 
        'type' => 'text', 
        'description' => t("The HTTP content type of the Activity Profile document."),
        'schema field' => 'content_type',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
      $info[$this->type]['properties']['contents'] = array(
        'label' => t("Document contents"), 
        'type' => 'text', 
        'description' => t("The binary data constituting the document."),
        'schema field' => 'contents',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      );
    return $info;
  }
}

/**
 * Provides TincanActivityProfileUIController for TincanActivityProfile entity
 */
class TincanActivityProfileUIController extends EntityContentUIController {
  /**
   * Implements hook_menu()
   */
   public function hook_menu() {
      $items['tincan-activity-profiles/' . '%'] = array(
        'page callback' => 'tincan_lrs_activity_profile_view',
        'page arguments' => array(1),
        'access callback' => 'tincan_lrs_tincan_activity_profile_access',
        'file' => 'ActivityProfile.php',
        'file path' => drupal_get_path('module','tincan_lrs') . '/includes',
       // 'type' => MENU_CALLBACK,
      );
     return $items;
  }
}

/**
 * Provides TincanActivityProfileExtraFieldsController for TincanActivityProfile entity
 */
class TincanActivityProfileExtraFieldsController extends EntityDefaultExtraFieldsController {
  protected $propertyInfo;

  /**
   * Implements EntityExtraFieldsControllerInterface::fieldExtraFields().
   */
  public function fieldExtraFields() {
    $extra = array();
    $this->propertyInfo = entity_get_property_info($this->entityType);
    if (isset($this->propertyInfo['properties'])) {
      foreach ($this->propertyInfo['properties'] as $name => $property_info) {
        // Skip adding the ID or bundle.
        if ($this->entityInfo['entity keys']['id'] == $name || $this->entityInfo['entity keys']['bundle'] == $name) {
          continue;
        }
        $extra[$this->entityType][$this->entityType]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
      }
    }
    // Handle bundle properties.
    $this->propertyInfo += array('bundles' => array());
    if (isset($this->propertyInfo['bundles'])) {
      foreach ($this->propertyInfo['bundles'] as $bundle_name => $info) {
        foreach ($info['properties'] as $name => $property_info) {
          if (empty($property_info['field'])) {
            $extra[$this->entityType][$bundle_name]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
          }
        }
      }
    }
    return $extra;
  }
}

/**
 * Provides TincanActivityProfileDefaultViewsController for TincanActivityProfile entities
 */
class TincanActivityProfileDefaultViewsController extends EntityDefaultViewsController {
  /**
   * {@inheritdoc}
   */
  public function views_data() {
    $data = parent::views_data();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  function schema_fields() {
    $data = parent::schema_fields();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  function map_from_schema_info($property_name, $schema_field_info, $property_info) {
    $return = parent::map_from_schema_info($property_name, $schema_field_info, $property_info);
    return $return;
  }
}

/**
 * Loads UI controller and generates view pages for Tincan Activity Profile entities
 *
 * Callback for hook_menu().
 *
 * @param integer id
 *
 * @return array $content
 *
 * @ingroup callbacks
 */
function tincan_lrs_activity_profile_view($id) {
  $content = "";
  $entity = entity_load('tincan_activity_profile', array($id));
  if (!empty($entity)) {
    $controller = entity_get_controller('tincan_activity_profile');
    $content = $controller->view($entity);
    drupal_set_title($entity[$id]->label());
  }
  else {
    drupal_set_title('Activity Profile not found.');
    $content = '<p>No agent found for drupal id: ' . $id . '</p>';
  }
  return $content;
}
