<?php
/**
 * @file
 * Configuration forms for the Tincan LRS module
 */

/**
 * Form constructor Tincan LRS settings page.
 *
 * @see tincan_lrs_settings_form_validate()
 * @see tincan_lrs_settings_form_submit()
 *
 * @ingroup forms
 */
function tincan_lrs_settings_form($form, &$form_state) {
  $form['heading'] = array(
    '#type' => 'markup',
    '#markup' => '<p>General or global settings will be made here</p>',
  );
  $form['#validate'][] = 'tincan_lrs_settings_form_validate';
  return system_settings_form($form);
}

/**
 * Form validation handler for tincan_lrs_settings_form().
 *
 * @see tincan_lrs_settings_form_submit()
 */
function tincan_lrs_settings_form_validate($form, &$form_state) {

}

/**
 * Form validation handler for tincan_lrs_settings_form().
 *
 * @see tincan_lrs_settings_form_submit()
 */
function tincan_lrs_settings_form_submit($form, &$form_state) {

}
