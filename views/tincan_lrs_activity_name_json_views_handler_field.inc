<?php
/**
 * @file
 * Views Activity entity Activity Name JSON handler
 */

/**
 * Provides Views Filter handler for activity name_json field
 */ 
class tincan_lrs_activity_name_json_views_handler_field extends views_handler_field {

  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['display_format'] = array('default' => 'full');
    $options['individual_key'] = array('default' => '');

    return $options;
  }

  /**
   * Options form
   *
   * @param $form
   * @param $form_state
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_format'] = array(
      '#type' => 'select',
      '#title' => 'Display Format',
      '#options' => array(
        'full' => 'Full JSON',
        'ind' => 'Individual Key',
      ),
      '#default_value' => $this->options['display_format'],
    );
    $form['individual_key'] = array(
      '#type' => 'textfield',
      '#title' => 'Individual Key',
      '#default_value' => $this->options['individual_key'],
    );
  }

  /**
   * Render the Activity Name JSON according to views handler settings
   * @param $values
   * @return string
   */
  function render($values) {
    $format = !empty($this->options['display_format']) ? $this->options['display_format'] : 'full';
    $key = !empty($this->options['individual_key']) ? $this->options['individual_key'] : '';
    if($format == 'ind' && !empty($values->tincan_activity_field_data_tincan_object_name_json)) {
      $json_array = drupal_json_decode($values->tincan_activity_field_data_tincan_object_name_json);
      return isset($json_array[$key]) ? $json_array[$key] : '';
    }
    else {
      return  parent::render($values);
    }
  }
}
