<?php
/**
 * @file
 * Views datetime field handler
 */

/**
 * Provides Views Filter handler for a datetime columns
 */ 
class tincan_lrs_handler_field_datetime extends views_handler_field_date {

  /**
   * Convert the DATETIME from the database into unixtime then allow
   * views_handler_field_date to render as usual.
   * Also trick php into thinking the time is in the same timezone, no
   * matter the default timezone
   *
   * @param $values
   *
   * @return string
   * rendering of values
   */
  function render($values) {
    $value = $values->{$this->field_alias};
    if (is_string($value) && strpos($value, "-")) {
      $value = strtotime($value);
      if ($value) {
        $values->{$this->field_alias} = $value;
      }
    }
    return parent::render($values);
  }
}
